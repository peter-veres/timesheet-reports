  module API
    class Timesheet < Grape::API
      before { authenticate! }

      desc 'Get timesheet data' do
        detail 'Timesheet data'
      end
      get '/timesheet' do
          type = params[:type]

          if type === 'projects'
            sql = 'select id, name from projects'
          end

          if type === 'users'
            sql = 'select id, name from users'
          end

          if type === 'load_results'
            where  = [];
            if params[:project]
              where.push('p.id = '+params[:project].to_i.to_s)
            end
            if params[:user]
              where.push('u.id = '+params[:user].to_i.to_s)
            end
            if params[:from]
              where.push('tl.spent_at >= \''+params[:from].gsub(/[^0-9\-]+/,'')+' 00:00:00\'')
            end
            if params[:to]
              where.push('tl.spent_at <= \''+params[:to].gsub(/[^0-9\-]+/,'')+' 23:59:59'+'\'')
            end
            
            sql = 'SELECT u.name as user_name, p.name as project_name, i.title as issue_title, i.iid issue_iid,string_agg(l.title,\'|\') as labels, tl.spent_at as spent_at, tl.time_spent as time_spent FROM timelogs AS tl INNER JOIN issues AS i oN i.id = tl.issue_id INNER JOIN projects p ON p.id = i.project_id INNER JOIN users u ON u.id = tl.user_id LEFT JOIN label_links ll on ll.target_id = i.id AND ll.target_type = \'Issue\' LEFT JOIN labels l on l.id = ll.label_id '
            if !where.empty?
              sql = sql + ' WHERE '+where.join(" AND ")
            end
            sql = sql + ' GROUP BY tl.id, i.id, u.id, p.id'
          end

          if defined? sql
            records_array = ActiveRecord::Base.connection.execute(sql)              
          else
            sql = ''
            records_array = []
          end

          { type: type, params: params,  query: sql, results: records_array }
      end
    end
  end
