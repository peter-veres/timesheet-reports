# GITLAB Timesheet Reports

Allows you to generate timesheet/timetracking reports from GitLab. 

**You need to have your own GitLab installed - will not work on hosted version of GitLab.**

Tested on Docker version of GitLab CE, running built-in Postgres version.

**This "plugin" adds its own API endpoint, so it can perform direct SQL queries on time logs and overcome limitations of GitLab's own time reporting APIs.**

**This "plugin" doesn't check project/issue access, so any user with valid API key can see all stats of all projects and all users**

## Install & Usage instructions

Following install instructions are for Docker version, but should work with other installation methods too.

1. Connect to Docker container, ie.: `docker exec -it <container_name> /bin/bash`
2. Open `api.rb` file (ie. `nano /opt/gitlab/embedded/service/gitlab-rails/lib/api/api.rb`) and append `mount ::API::Timesheet` after `mount ::API::Wikis` at the bottom of the file (save and close)
3. Create file `/opt/gitlab/embedded/service/gitlab-rails/lib/api/timesheet.rb` and insert contents of `timesheet.rb` file from this repository
4. Restart GitLab instance with `gitlab-ctl restart` (might take few minutes to start responding again, meanwhile you will see 502 error)
5. Login to GitLab and create Personal Access Token (Top Right profile menu > Settings > Access Tokens) with `api` access. Write down the token.
6. Open the file `timesheet-reports.html` in your browser, enter the token and your GitLab API URL in the inputs on the right.

... and you should be ready to go ...

![screenshot](https://gitlab.com/peter-veres/timesheet-reports/raw/21f5be94e6720dffe901e37352cf3621f91ca7b1/screenshot.png)

Uses jQuery, DataTables, PivotTable.js